using AdvertisementSystem.Services;
using AdvertisementSystem.Services.Interfaces;
using AdvertisementSystem.Utils.Errors;
using AdvertisementSystem.Utils.Errors.Interfaces;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddJsonFile("appsettings.json");

builder.Services.AddMemoryCache();

builder.Services.AddSingleton<IAdvertisementErrorProvider, AdvertisementErrorProvider>();
    
builder.Services.AddScoped<IAdvertisementService, AdvertisementService>();

WebApplication app = builder.Build();

app.Run();