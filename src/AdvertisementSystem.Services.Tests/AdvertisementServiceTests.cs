﻿using AdvertisementSystem.Services;
using AdvertisementSystem.Utils;
using AdvertisementSystem.Utils.Errors.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using ThirdParty;
using Xunit;

namespace AdvertisementSystem.Service.Tests;

public class AdvertisementServiceTests
{
    [Fact]
    public void GetAdvertisement_WithAvailableCache()
    {
        // Arrange
        Advertisement cachedAdvertisement = new Advertisement { WebId = "123", Name = "Mocked Advertisement", Description = "TestDescription" };
        
        Mock<IAdvertisementErrorProvider> advertisementErrorProvider = new Mock<IAdvertisementErrorProvider>();
        Stack<AdvertisementError> errors = new();
        advertisementErrorProvider.SetupGet(e => e.Errors).Returns(errors);
        
        Mock<IConfiguration> configurationMock = new Mock<IConfiguration>();
        Mock<IMemoryCache> cacheMock = new Mock<IMemoryCache>();
        Mock<ILogger<AdvertisementService>> loggerMock = new Mock<ILogger<AdvertisementService>>();

        cacheMock.Setup(x => x.TryGetValue(It.IsAny<object>(), out It.Ref<object>.IsAny))
            .Returns((object key, out object value) =>
            {
                value = cachedAdvertisement;
                return true;
            });

        AdvertisementService advertisementService = new AdvertisementService(advertisementErrorProvider.Object, configurationMock.Object, cacheMock.Object, loggerMock.Object);
        
        // Act
        Advertisement advertisement = advertisementService.GetAdvertisement("123");

        // Assert
        Assert.NotNull(advertisement);
        Assert.Equal("123", advertisement.WebId);
        Assert.Equal("Mocked Advertisement", advertisement.Name);
        Assert.Equal("TestDescription", advertisement.Description);
    }
    
    [Fact]
    public void GetAdvertisement_WithEmptyCache_And_No_Errors()
    {
        // Arrange
        Mock<IAdvertisementErrorProvider> advertisementErrorProvider = new Mock<IAdvertisementErrorProvider>(); 
        Stack<AdvertisementError> errors = new();
        advertisementErrorProvider.SetupGet(e => e.Errors).Returns(errors);
        
        Mock<IConfiguration> configurationMock = new Mock<IConfiguration>();
        configurationMock.Setup(x => x.GetSection("Advertisement:NoSQLRetryCount").Value).Returns("3");

        MemoryCache cacheMock = new MemoryCache(new MemoryCacheOptions());
        Mock<ILogger<AdvertisementService>> loggerMock = new Mock<ILogger<AdvertisementService>>();

        AdvertisementService advertisementService = new AdvertisementService(advertisementErrorProvider.Object, configurationMock.Object, cacheMock, loggerMock.Object);

        // Act
        Advertisement advertisement = advertisementService.GetAdvertisement("123");

        // Assert
        Assert.NotNull(advertisement);
        Assert.Equal("123", advertisement.WebId);
        Assert.Equal("Advertisement #123", advertisement.Name);
        Assert.Null(advertisement.Description);
    }
    
    [Fact]
    public void GetAdvertisement_WithEmptyCache_And_Errors_5()
    {
        // Arrange
        Mock<IAdvertisementErrorProvider> advertisementErrorProvider = new Mock<IAdvertisementErrorProvider>(); 
        Stack<AdvertisementError> errors = new();
        advertisementErrorProvider.SetupGet(e => e.Errors).Returns(errors);
        
        Mock<IConfiguration> configurationMock = new Mock<IConfiguration>();
        configurationMock.Setup(x => x.GetSection("Advertisement:NoSQLRetryCount").Value).Returns("3");

        MemoryCache cacheMock = new MemoryCache(new MemoryCacheOptions());
        Mock<ILogger<AdvertisementService>> loggerMock = new Mock<ILogger<AdvertisementService>>();

        AdvertisementService advertisementService = new AdvertisementService(advertisementErrorProvider.Object, configurationMock.Object, cacheMock, loggerMock.Object);
        for (int index = 0; index < 5; index++)
        {
            advertisementErrorProvider.Object.Errors.Push(new AdvertisementError { TimeStamp = DateTimeOffset.UtcNow.AddMinutes(-index) } );
        }

        // Act
        Advertisement advertisement = advertisementService.GetAdvertisement("123");

        // Assert
        Assert.NotNull(advertisement);
        Assert.Equal("123", advertisement.WebId);
        Assert.Equal("Advertisement #123", advertisement.Name);
        Assert.Null(advertisement.Description);
    }
    
    [Fact]
    public void GetAdvertisement_WithEmptyCache_And_Errors_More_Than_10_But_Less_Than_20()
    {
        // Arrange
        Mock<IAdvertisementErrorProvider> advertisementErrorProvider = new Mock<IAdvertisementErrorProvider>();
        Stack<AdvertisementError> errors = new();
        advertisementErrorProvider.SetupGet(e => e.Errors).Returns(errors);
        
        Mock<IConfiguration> configurationMock = new Mock<IConfiguration>();
        configurationMock.Setup(x => x.GetSection("Advertisement:NoSQLRetryCount").Value).Returns("3");

        MemoryCache cacheMock = new MemoryCache(new MemoryCacheOptions());
        Mock<ILogger<AdvertisementService>> loggerMock = new Mock<ILogger<AdvertisementService>>();
        
        AdvertisementService advertisementService = new AdvertisementService(advertisementErrorProvider.Object, configurationMock.Object, cacheMock, loggerMock.Object);
        for (int index = 0; index < 10; index++)
        {
            advertisementErrorProvider.Object.Errors.Push(new AdvertisementError { TimeStamp = DateTimeOffset.UtcNow.AddMinutes(-index) } );
        }
        
        // Act
        Advertisement advertisement = advertisementService.GetAdvertisement("123");

        // Assert
        Assert.NotNull(advertisement);
        Assert.Equal("123", advertisement.WebId);
        Assert.Equal("Advertisement #123", advertisement.Name);
        Assert.Null(advertisement.Description);
    }
    
    [Fact]
    public void GetAdvertisement_WithEmptyCache_And_Errors_More_Than_20()
    {
        // Arrange
        Mock<IAdvertisementErrorProvider> advertisementErrorProvider = new Mock<IAdvertisementErrorProvider>();
        Stack<AdvertisementError> errors = new();
        advertisementErrorProvider.SetupGet(e => e.Errors).Returns(errors);
        
        Mock<IConfiguration> configurationMock = new Mock<IConfiguration>();
        configurationMock.Setup(x => x.GetSection("Advertisement:NoSQLRetryCount").Value).Returns("3");

        MemoryCache cacheMock = new MemoryCache(new MemoryCacheOptions());
        Mock<ILogger<AdvertisementService>> loggerMock = new Mock<ILogger<AdvertisementService>>();
        
        AdvertisementService advertisementService = new AdvertisementService(advertisementErrorProvider.Object, configurationMock.Object, cacheMock, loggerMock.Object);
        for (int index = 0; index < 30; index++)
        {
            advertisementErrorProvider.Object.Errors.Push(new AdvertisementError { TimeStamp = DateTimeOffset.UtcNow.AddMinutes(-index) } );
        }
        
        // Act
        Advertisement advertisement = advertisementService.GetAdvertisement("123");

        // Assert
        Assert.NotNull(advertisement);
        Assert.Equal("123", advertisement.WebId);
        Assert.Equal("Advertisement #123", advertisement.Name);
        Assert.Null(advertisement.Description);
    }
}