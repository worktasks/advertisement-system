﻿using AdvertisementSystem.Services.Interfaces;
using AdvertisementSystem.Utils;
using AdvertisementSystem.Utils.Errors.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ThirdParty;

namespace AdvertisementSystem.Services;

public class AdvertisementService : IAdvertisementService
{
    private readonly IAdvertisementErrorProvider _advertisementErrorProvider;
    private readonly IConfiguration _configuration;
    private readonly IMemoryCache _cache;
    private readonly ILogger<AdvertisementService> _logger;

    public AdvertisementService(IAdvertisementErrorProvider advertisementErrorProvider, IConfiguration configuration, 
        IMemoryCache cache, ILogger<AdvertisementService> logger)
    {
        _advertisementErrorProvider = advertisementErrorProvider;
        _cache = cache;
        _configuration = configuration;
        _logger = logger;
    }
    
    // **************************************************************************************************
    // Loads Advertisement information by id
    // from cache or if not possible uses the "mainProvider" or if not possible uses the "backupProvider"
    // **************************************************************************************************
    // Detailed Logic:
    // 
    // 1. Tries to use cache (and retuns the data or goes to STEP2)
    //
    // 2. If the cache is empty it uses the NoSqlDataProvider (mainProvider), 
    //    in case of an error it retries it as many times as needed based on AppSettings
    //    (returns the data if possible or goes to STEP3)
    //
    // 3. If it can't retrieve the data or the ErrorCount in the last hour is more than 10, 
    //    it uses the SqlDataProvider (backupProvider)
    public Advertisement GetAdvertisement(string id)
    {
        // Use Cache if available
        Advertisement advertisement = (Advertisement) _cache.Get($"AdvKey_{id}");

        // If Cache is empty and ErrorCount < 10 then use HTTP provider
        int errorsForLastHourCount = GetErrorsForLastHour();
        if (advertisement == null && errorsForLastHourCount < 10)
        {
            advertisement = GetAdvertisementFromMainProvider(id);
        }

        // if needed try to use Backup provider
        if (advertisement == null)
        {
            advertisement = GetAdvertisementFromBackUpProvider(id);
        }

        return advertisement;
    }

    private int GetErrorsForLastHour()
    {
        int errorsForLastHourCount = 0;
        int length = _advertisementErrorProvider.Errors.Count > 20 ? 20 : _advertisementErrorProvider.Errors.Count; 
        for (int index = 0; index < length; index++)
        {
            if (_advertisementErrorProvider.Errors.Pop().TimeStamp > DateTimeOffset.UtcNow.AddHours(-1))
            {
                errorsForLastHourCount++;
            }
        }
        
        return errorsForLastHourCount;
    }

    private Advertisement GetAdvertisementFromMainProvider(string id)
    {
        Advertisement advertisement = null;
        
        int noSQLRetryCount = int.Parse(_configuration.GetSection("Advertisement:NoSQLRetryCount").Value);
        for (int retry = 0; retry < noSQLRetryCount; retry++)
        {
            try
            {
                NoSqlAdvProvider dataProvider = new();
                advertisement = dataProvider.GetAdv(id);
            }
            catch
            {
                Thread.Sleep(1000);
                _advertisementErrorProvider.Errors.Push(new AdvertisementError { TimeStamp = DateTimeOffset.UtcNow });
                _logger.LogError($"Failed to get advertisement from NoSqlAdvProvider!");
            }
        } 
            
        if (advertisement != null)
        {
            _cache.Set($"AdvKey_{id}", advertisement, DateTimeOffset.UtcNow.AddMinutes(5));
        }
        
        _logger.LogInformation("Successfully receiving advertisement from NoSQLProvider and adding it to cache!");
        return advertisement;
    }
    
    private Advertisement GetAdvertisementFromBackUpProvider(string id)
    {
        Advertisement advertisement = SQLAdvProvider.GetAdv(id);
        if (advertisement != null)
        {
            _cache.Set($"AdvKey_{id}", advertisement, DateTimeOffset.UtcNow.AddMinutes(5));
        }
        
        _logger.LogInformation("Successfully receiving advertisement from SQLProvider and adding it to cache!");
        return advertisement;
    }
}