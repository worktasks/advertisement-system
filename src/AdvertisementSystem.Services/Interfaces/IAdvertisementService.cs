﻿using ThirdParty;

namespace AdvertisementSystem.Services.Interfaces;

public interface IAdvertisementService
{
    public Advertisement GetAdvertisement(string id);
}