﻿namespace AdvertisementSystem.Utils;

public class AdvertisementError
{
    public string Message { get; set; }

    public DateTimeOffset TimeStamp { get; set; }
}