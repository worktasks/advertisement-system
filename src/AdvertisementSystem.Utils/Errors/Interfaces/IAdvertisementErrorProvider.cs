﻿namespace AdvertisementSystem.Utils.Errors.Interfaces;

public interface IAdvertisementErrorProvider
{
    public Stack<AdvertisementError> Errors => new();
}