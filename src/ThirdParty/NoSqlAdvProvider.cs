﻿namespace ThirdParty;

public class NoSqlAdvProvider
{
    public Advertisement GetAdv(string webId)
    {
        return new Advertisement { WebId = webId, Name = $"Advertisement #{webId}" };
    }
}