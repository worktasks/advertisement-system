﻿namespace ThirdParty;

public static class SQLAdvProvider
{
    public static Advertisement GetAdv(string webId)
    {
        return new Advertisement { WebId = webId, Name = $"Advertisement #{webId}" };
    }
}